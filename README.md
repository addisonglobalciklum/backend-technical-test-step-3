# 3. REST API

**Task**: Define a simple REST API to offer the functionality of the SimpleAsyncTokenService implemented in the previous block.

For its implementation we prefer you to use Akka HTTP, however, it's not mandatory and you can use other frameworks such as http4s, Play Framework or Spring Boot.

##### Technical Consideration:

- I choose Playframework because It's a complete framework to increase time to market and It's have an async controller to manage highly concurrency

## Running

Run this using [sbt](http://www.scala-sbt.org/).  If you downloaded this project from <http://www.playframework.com/download> then you'll find a prepackaged version of sbt in the project directory:

```bash
sbt run
```

And then go to <http://localhost:9000/user/authenticate/request-token/house> to see the running web application.

## Testing
```bash
sbt test
```