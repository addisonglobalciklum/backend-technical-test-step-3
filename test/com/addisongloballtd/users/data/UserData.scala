package com.addisongloballtd.users.data

import src.main.scala.com.addisongloballtd.users.models.{Credentials, User}

trait UserData {
  val credentials = Credentials("house", "HOUSE")
  val user        = User(credentials.username)

  val invalidCredentials = Credentials("house", "HOUSe")
  val invalidUser        = User(invalidCredentials.username)

  val credentialsStartA = Credentials("Ahouse", "HOUSE")
  val userStartA        = User(credentialsStartA.username)

}
object UserData extends UserData
