package com.addisongloballtd.users.actors

import akka.actor.{ActorSystem, Props}
import akka.testkit.{TestKit, TestProbe}
import com.addisongloballtd.users.data.UserData
import org.joda.time.{DateTime, DateTimeUtils, DateTimeZone}
import org.scalatest.{BeforeAndAfterAll, FlatSpecLike, MustMatchers}
import src.main.scala.com.addisongloballtd.users.actors.IssueToken
import src.main.scala.com.addisongloballtd.users.actors.IssueToken.GenerateToken
import src.main.scala.com.addisongloballtd.users.models.{InvalidUserToken, ValidUserToken}
import scala.concurrent.duration._

class IssueTokenSpec extends TestKit(ActorSystem("test-system"))
                     with FlatSpecLike
                     with BeforeAndAfterAll
                     with MustMatchers
                     with UserData
{
  override def beforeAll(): Unit = {
    DateTimeUtils.setCurrentMillisFixed(DateTime.now(DateTimeZone.UTC).getMillis)
  }

  override def afterAll(): Unit = {
    DateTimeUtils.setCurrentMillisSystem()
    TestKit.shutdownActorSystem(system)
  }

  "IssueTokenSpec" should "Generate Token" in {
    //arrange
    val sender        = TestProbe()
    val issueToken    = system.actorOf(Props[IssueToken])
    val time          = DateTime.now(DateTimeZone.UTC)
    val generateToken = GenerateToken(user, sender.ref)

    //act
    issueToken ! generateToken

    //assert
    sender.expectMsg(5000 millis, ValidUserToken(user.userId+"_"+ time.toString()))
  }

  "IssueTokenSpec" should "can't Generate Token" in {
    //arrange
    val sender        = TestProbe()
    val issueToken    = system.actorOf(Props[IssueToken])
    val time          = DateTime.now(DateTimeZone.UTC)
    val generateToken = GenerateToken(userStartA, sender.ref)

    //act
    issueToken ! generateToken

    //assert
    sender.expectMsg(5000 millis, InvalidUserToken(userStartA))
  }

}
