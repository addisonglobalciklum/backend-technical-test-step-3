package com.addisongloballtd.users.actors

import akka.actor.{ActorSystem, Props}
import akka.testkit.{TestKit, TestProbe}
import com.addisongloballtd.users.data.UserData
import org.scalatest.{BeforeAndAfterAll, FlatSpecLike, MustMatchers}
import src.main.scala.com.addisongloballtd.users.actors.UserSupervisor
import src.main.scala.com.addisongloballtd.users.actors.UserSupervisor.ProcessToken

import scala.concurrent.duration._

class UserSupervisorSpec extends TestKit(ActorSystem("test-system"))
  with FlatSpecLike
  with BeforeAndAfterAll
  with MustMatchers
  with UserData
{
  override def afterAll(): Unit = {
    TestKit.shutdownActorSystem(system)
  }

  "UserSupervisorSpec" should "Generate Token" in {
    //arrange
    val sender = TestProbe()
    val userSupervisor = system.actorOf(Props[UserSupervisor])
    val issueToken  = system.actorSelection("/user/issue-token-pool")
    val auth        = system.actorSelection("/user/auth-pool")

    val processToken = ProcessToken(credentials, auth, issueToken)

    //act
    userSupervisor ! processToken

    //assert
    sender.expectNoMsg(1.second)
  }

}
