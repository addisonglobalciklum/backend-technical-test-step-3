package com.addisongloballtd.users.actors

import akka.actor.{ActorSystem, Props}
import akka.testkit.{TestKit, TestProbe}
import com.addisongloballtd.users.data.UserData
import org.scalatest.{BeforeAndAfterAll, FlatSpecLike, MustMatchers}
import src.main.scala.com.addisongloballtd.users.actors.Authenticate
import src.main.scala.com.addisongloballtd.users.actors.Authenticate.Validate

import scala.concurrent.duration._

class AuthenticateSpec extends TestKit(ActorSystem("test-system"))
  with FlatSpecLike
  with BeforeAndAfterAll
  with MustMatchers
  with UserData
{

  override def afterAll(): Unit = {
    TestKit.shutdownActorSystem(system)
  }

  "AuthenticateSpec" should "Generate Token" in {
    //arrange
    val sender      = TestProbe()
    val auth        = system.actorOf(Props[Authenticate])
    val issueToken  = system.actorSelection("/user/issue-token-pool")
    val validate    = Validate(credentials, sender.ref, issueToken)

    auth ! validate

    sender.expectNoMsg(1.second)
  }

  "AuthenticateSpec" should "Can't Generate Token" in {
    //arrange
    val sender      = TestProbe()
    val auth        = system.actorOf(Props[Authenticate])
    val issueToken  = system.actorSelection("/user/issue-token-pool")
    val validate    = Validate(invalidCredentials, sender.ref, issueToken)

    auth ! validate

    sender.expectNoMsg(1.second)
  }

}
