package src.main.scala.com.addisongloballtd.users.json

import play.api.libs.json.Json
import src.main.scala.com.addisongloballtd.users.models.{InvalidUserToken, User, UserToken, ValidUserToken}


object Formats {
  implicit val validUserTokenFormat = Json.format[ValidUserToken]
  implicit val userFormat = Json.format[User]
  implicit val invalidUserTokenFormat = Json.format[InvalidUserToken]
  //implicit val userTokenFormat = Json.writes[UserToken]
}
