package src.main.scala.com.addisongloballtd.users

import akka.actor.ActorSystem
import src.main.scala.com.addisongloballtd.users.json.Formats._
import javax.inject.{Inject, Singleton}
import play.api.libs.json.Json
import play.api.mvc.{AbstractController, ControllerComponents}
import src.main.scala.com.addisongloballtd.users.models.{Credentials, InvalidUserToken, UserToken, ValidUserToken}
import src.main.scala.com.addisongloballtd.users.services.SimpleAsyncTokenServiceProvider

import scala.concurrent.{ExecutionContext, Future}

@Singleton
class SimpleAsyncTokenServiceController @Inject()(cc: ControllerComponents,
                                                  actorSystem: ActorSystem
                                                   )(implicit exec: ExecutionContext)
  extends AbstractController(cc)
  with SimpleAsyncTokenServiceProvider
{

  def requestToken(userId: String) = Action.async {
    val credentials = Credentials(userId, userId.toUpperCase())
    val userTokenF: Future[UserToken] = simpleAsyncTokenService.requestToken(credentials)

    userTokenF.map{ userToken =>
      userToken match {
        case ValidUserToken(token)  => Ok(Json.toJson(token))
        case InvalidUserToken(user) => BadRequest(Json.toJson(user))
      }
    }.recover{
      case error => InternalServerError
    }
  }

}
