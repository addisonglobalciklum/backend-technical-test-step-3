package src.main.scala.com.addisongloballtd.users.models

trait UserToken
case class ValidUserToken(token: String) extends UserToken
case class InvalidUserToken(user: User) extends UserToken