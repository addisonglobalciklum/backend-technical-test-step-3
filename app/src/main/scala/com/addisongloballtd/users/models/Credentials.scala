package src.main.scala.com.addisongloballtd.users.models

case class Credentials(username: String, password: String)