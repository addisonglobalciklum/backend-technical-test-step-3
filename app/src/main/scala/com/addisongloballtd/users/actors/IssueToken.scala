package src.main.scala.com.addisongloballtd.users.actors

import akka.actor.{Actor, ActorLogging, ActorRef}
import org.joda.time.{DateTime, DateTimeZone}
import src.main.scala.com.addisongloballtd.users.actors.IssueToken.{GenerateToken, InvalidUser, UserStatus, ValidUser}
import src.main.scala.com.addisongloballtd.users.models.{InvalidUserToken, User, ValidUserToken}

import scala.util.Random

object IssueToken {
  sealed trait IssueTokenMsg
  case class GenerateToken(user: User, responseTo: ActorRef) extends IssueTokenMsg

  sealed trait UserStatus
  final case object ValidUser extends UserStatus
  final case object InvalidUser extends UserStatus
}

class IssueToken extends Actor with ActorLogging{

  override def preStart(): Unit = {
    //log.info(s"IssueToken up ${self.path}")
  }

  override def postStop(): Unit = {
    //log.info(s"IssueToken stop")
  }


  def receive = {
    case GenerateToken(user, responseTo) => {
      log.info(s"issue actor GenerateToken $user")
      Thread.sleep(Random.nextInt(5000))
      validateStartLetter(user) match {
        case ValidUser => {
          log.info(s"issue actor ValidUser $responseTo")
          val userToken = ValidUserToken(user.userId + "_" + DateTime.now(DateTimeZone.UTC).toString())
          responseTo ! userToken
        }
        case InvalidUser => { //todo: pendiente por dar manejo
          log.info(s"issue actor validateStartLetter false")
          responseTo ! InvalidUserToken(user)
        }
      }
    }
  }

  private def validateStartLetter(user: User) : UserStatus = {
    if(user.userId.startsWith("A"))
      InvalidUser
    else
      ValidUser
  }

}
