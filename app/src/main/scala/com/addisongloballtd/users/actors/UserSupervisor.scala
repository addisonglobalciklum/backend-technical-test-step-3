package src.main.scala.com.addisongloballtd.users.actors

import akka.actor.SupervisorStrategy.{Escalate, Restart, Resume, Stop}
import akka.actor.{Actor, ActorLogging, ActorSelection, OneForOneStrategy}
import src.main.scala.com.addisongloballtd.users.actors.Authenticate.Validate
import src.main.scala.com.addisongloballtd.users.actors.SupervisorException.{RestartException, ResumeException, StopException}
import src.main.scala.com.addisongloballtd.users.actors.UserSupervisor.ProcessToken
import src.main.scala.com.addisongloballtd.users.models.{Credentials, User, UserToken}

object UserSupervisor{
  sealed trait Commands
  final case class ProcessToken(credentials: Credentials, tellToAuth: ActorSelection, tellToIssueToken: ActorSelection) extends Commands
  final case class ReceiveToken(user: User, userToken: UserToken) extends Commands
  final case class FindToken(user: User) extends Commands
}

object SupervisorException {
  case object ResumeException extends Exception
  case object StopException extends Exception
  case object RestartException extends Exception
}


class UserSupervisor() extends Actor with ActorLogging{

  import scala.concurrent.duration._

  override val supervisorStrategy =
    OneForOneStrategy(maxNrOfRetries = 10, withinTimeRange = 1 seconds) {
      case ResumeException  => Resume
      case RestartException => Restart
      case StopException    => Stop
      case _ : Exception    => Escalate

    }

  override def preStart(): Unit = {
    //log.info(s"UserSupervisor up ")
    //val issueToken   = context.actorOf(Props[IssueToken], "issueTokenSup")
    //val authChildRef = authenticate
  }

  override def postStop(): Unit = {
    log.info(s"UserSupervisor stop")
  }

  def receive: Receive = {
    case ProcessToken(credentials, tellToAuth, tellToIssueToken) => {
      log.info(s"UserSupervisor credentials : $credentials")
      tellToAuth ! Validate(credentials, sender(), tellToIssueToken)
    }
  }

}
