package src.main.scala.com.addisongloballtd.users.services

import akka.pattern.ask
import akka.util.Timeout
import src.main.scala.com.addisongloballtd.users.actors.UserSupervisor.ProcessToken
import src.main.scala.com.addisongloballtd.users.models.{Credentials, UserToken}

import scala.concurrent.{Await, Future}

trait SimpleAsyncTokenServiceImpl extends SimpleAsyncTokenService with SimpleAsyncTokenActorSystemProvider{

  import scala.concurrent.duration._
  implicit val timeout = Timeout(11000.millisecond)

  def requestToken(credentials: Credentials): Future[UserToken] = {

    println(s"siiiiiiiii llego requestToken")

    val actorSystem = simpleAsyncTokenActorSystem.startActors()

    val issueToken   = actorSystem.actorSelection("/user/issue-token-pool")
    val authenticate = actorSystem.actorSelection("/user/auth-pool")
    val supervisor   = actorSystem.actorSelection("/user/supervisor-pool")

    (supervisor ? ProcessToken(credentials, authenticate, issueToken)).mapTo[UserToken]
  }

}

object SimpleAsyncTokenServiceImpl extends SimpleAsyncTokenServiceImpl