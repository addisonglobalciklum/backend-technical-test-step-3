package src.main.scala.com.addisongloballtd.users.services

trait SimpleAsyncTokenActorSystemProvider {
  def simpleAsyncTokenActorSystem : SimpleAsyncTokenActorSystem = SimpleAsyncTokenActorSystem
}
