package src.main.scala.com.addisongloballtd.users.services

import akka.actor.{ActorSystem, Props}
import akka.routing.FromConfig
import src.main.scala.com.addisongloballtd.users.actors.{Authenticate, IssueToken, UserSupervisor}

trait SimpleAsyncTokenActorSystem {

  def startActors() : ActorSystem = {
    val actorSystem = ActorSystem("issue-token-random")

    //round robin router
    //val issueToken   = actorSystem.actorOf(RoundRobinPool(3).props(Props[IssueToken]), "issueToken")
    //val authenticate = actorSystem.actorOf(RoundRobinPool(3).props(Props(classOf[Authenticate])), "auth-pool")
    //val supervisor   = actorSystem.actorOf(RoundRobinPool(3).props(Props(classOf[UserSupervisor])), "supervisor-pool")

    //random router
    println(s"startActors init")
    val issueTokenPool   = actorSystem.actorOf(FromConfig.props(Props[IssueToken]), "issue-token-pool")
    val authenticatePool = actorSystem.actorOf(FromConfig.props(Props(classOf[Authenticate])), "auth-pool")
    val supervisorPool   = actorSystem.actorOf(FromConfig.props(Props(classOf[UserSupervisor])), "supervisor-pool")
    println(s"startActors end $issueTokenPool")

    actorSystem
  }

}

object SimpleAsyncTokenActorSystem extends SimpleAsyncTokenActorSystem
