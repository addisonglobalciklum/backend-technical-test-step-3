package src.main.scala.com.addisongloballtd.users.services

trait SimpleAsyncTokenServiceProvider {
  def simpleAsyncTokenService : SimpleAsyncTokenService = SimpleAsyncTokenServiceImpl
}
