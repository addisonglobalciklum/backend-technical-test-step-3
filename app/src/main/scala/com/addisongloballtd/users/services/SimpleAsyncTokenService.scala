package src.main.scala.com.addisongloballtd.users.services

import src.main.scala.com.addisongloballtd.users.models.{Credentials, UserToken}

import scala.concurrent.Future

trait SimpleAsyncTokenService {
  def requestToken(credentials: Credentials): Future[UserToken]
}
